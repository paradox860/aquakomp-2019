//***Функция считывания температуры c Далласов*****
void dallRead(unsigned long interval) {
  static unsigned long prevTime = 0;
  if (millis() - prevTime > interval) { //Проверка заданного интервала
    static boolean flagDall = 0; //Признак операции
    prevTime = millis();
    flagDall = ! flagDall; //Инверсия признака
    if (flagDall) {
      ///////////
      if ( !ds.search(addr)) {
        //no more sensors on chain, reset search
        ds.reset_search();
        return +1000;
      }
      if ( addr[0] != 0x10 && addr[0] != 0x28) {
      //  Serial.print("Device is not recognized");
        return -1000;
      }
      ds.reset(); // сброс шины
      ds.select(0xCC); //выставить адрес
      ds.write(0x4E); // разрешение записать конфиг
      ds.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
      ds.write(0xFF); //Tl контроль температуры мин -128грд
   //   ds.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
   
  /*   ds.reset(); // сброс шины
      ds.select(0xCC); //выставить адрес
      ds.write(0x4E); // разрешение записать конфиг
      ds.write(0x00); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
      ds.write(0x00); //Tl контроль температуры мин -128грд
      ds.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F*/
      ////////////
      ds.reset();
      ds.write(0xCC); //Обращение ко всем датчикам
      ds.write(0x44, 1); //Команда на конвертацию 1 -паразитное питание
      flagDallRead = 1; //Время возврата в секундах
    }
    else {
      ds.reset();
      ds.select(addr); // обращаемся по адресу
      ds.write(0xBE, 1); // чтение из памяти
      for (int i = 0; i < 9; i++) { // we need 9 bytes
        data[i] = ds.read ();
      }
      ds.reset_search();
      int16_t raw = ((data[1] << 8) | data[0]);//=======Пересчитываем в температуру
      Temp_val = (float)raw / 16.0;
    }
  }
}
//--------------------------------------------------
//***Функция считывания температуры c Далласов*****
void dallRead_led(unsigned long interval) {
  static unsigned long prevTime = 0;
  if (millis() - prevTime > interval) { //Проверка заданного интервала
    static boolean flagDall = 0; //Признак операции
    prevTime = millis();
    flagDall = ! flagDall; //Инверсия признака
    if (flagDall) {
      ///////////
      if ( !ds_led.search(addr_led)) {
        //no more sensors on chain, reset search
        ds_led.reset_search();
        return +1000;
      }
      if ( addr_led[0] != 0x10 && addr_led[0] != 0x28) {
      //  Serial.print("Device is not recognized");
        return -1000;
      }
      ds_led.reset(); // сброс шины
      ds_led.select(0xCC); //выставить адрес
      ds_led.write(0x4E); // разрешение записать конфиг
      ds_led.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
      ds_led.write(0xFF); //Tl контроль температуры мин -128грд
   //   ds.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
   
  /*   ds.reset(); // сброс шины
      ds.select(0xCC); //выставить адрес
      ds.write(0x4E); // разрешение записать конфиг
      ds.write(0x00); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F
      ds.write(0x00); //Tl контроль температуры мин -128грд
      ds.write(0x7F); // точность 0,5гр = 1F; 0,25гр = 3F; 0,125гр = 5F; 0,0625гр = 7F*/
      ////////////
      ds_led.reset();
      ds_led.write(0xCC); //Обращение ко всем датчикам
      ds_led.write(0x44, 1); //Команда на конвертацию 1 -паразитное питание
      flagDallRead_led = 1; //Время возврата в секундах
    }
    else {
      ds_led.reset();
      ds_led.select(addr_led); // обращаемся по адресу
      ds_led.write(0xBE, 1); // чтение из памяти
      for (int i = 0; i < 9; i++) { // we need 9 bytes
        data_led[i] = ds_led.read ();
      }
      ds_led.reset_search();
      int16_t raw = ((data_led[1] << 8) | data_led[0]);//=======Пересчитываем в температуру
      Temp_val_led = (float)raw / 16.0;
    }
  }
}
//--------------------------------------------------
