void MAIN()
{
  // counter++;
  if (enc1.isPress() || enc1.isTurn()) {
    counter = 0;
    flag = 1;
    lcd_count = 0;
  }
  if (menu_val != 1) {
    counter++;
  }
  if (counter == DELAY_MENU ) {
    lcd.clear();
    counter = 0;
    menu_val = 1;
    menu_val_2 = 0;
    hr = 0;
    EEPROM_WR();
  }

  if (enc1.isClick())
  {
    menu_val_2 = 0;
    hr = 0;
    EEPROM_WR();
    lcd.clear();
    if (menu_val < 16)
    {
      menu_val++;
    }

    else {
      menu_val = 1;
    }


  }
  // menu_val = 7;
  ///////////////////////////////////////////////////////////////////////////////////меню
  switch (menu_val) {

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////основной экран
    case 1:

      lcd.setCursor(0, 0);
      if (h < 10) {
        lcd.print("0");
      }
      lcd.print(h);
      if (s % 10 % 2 == 0) {
        lcd.print(":");
      } else {
        lcd.print(" ");
      }
      if (m < 10) {
        lcd.print("0");
      }
      lcd.print(m);

      ///////////LED
      lcd.setCursor(6, 0);
      /*switch (rejim_dnya) {
        case 0:
          lcd.print("  ");
          break;
        case 1:
          lcd.write(4);
          lcd.write(6);
          break;
        case 2:
          lcd.print(" ");
          // lcd.write(1);
          lcd.write(6);
          break;
        case 3:
          lcd.write(5);
          lcd.write(6);
          break;
        case 4:
          lcd.write(4);
          lcd.write(7);
          break;
        case 5:
          lcd.print(" ");
          //   lcd.write(1);
          lcd.write(7);
          break;
        case 6:
          lcd.write(5);
          lcd.write(7);
          break;
        }*/
      switch (rejim_dnya) {
        case 0:
          lcd.print("__");
          break;
        case 1:
          lcd.write(4);
          lcd.write(6);
          break;
        case 2:
          lcd.print(" ");
          // lcd.write(1);
          lcd.write(6);
          break;
        case 3:
          lcd.write(5);
          lcd.write(6);
          break;
        case 4:
          lcd.write(4);
          lcd.write(7);
          break;
        case 5:
          lcd.print(" ");
          //   lcd.write(1);
          lcd.write(7);
          break;
        case 6:
          lcd.write(5);
          lcd.write(7);
          break;
        case 7:
          lcd.print(" ");
          lcd.write(2);
          break;
      }
      //////////LED
      //////////температура
      lcd.setCursor(10, 0);
      if ((Temp_val - x_temp) > delta)
      {
        lcd.print("*") ;
        digitalWrite(cool_pin, HIGH);
      }
      if ((x_temp - Temp_val) > delta)
      {
        lcd.print(" ") ;
        digitalWrite(cool_pin, LOW);
      }


      lcd.setCursor(11, 0);
      lcd.print(Temp_val, 1);
      lcd.write(223);

      //////////температура
    
      /////////////////////////////////каналы
      if (ch_1[(h * 60 + m) / 30] == 1)
      {
        digitalWrite(ch_1_pin, ON);
      } else {
        digitalWrite(ch_1_pin, OFF);
      }
      if (ch_2[(h * 60 + m) / 30] == 1)
      {
        digitalWrite(ch_2_pin, ON);
      } else {
        digitalWrite(ch_2_pin, OFF);
      }
      if (ch_3[(h * 60 + m) / 30] == 1)
      {
        digitalWrite(ch_3_pin, ON);
      } else {
        digitalWrite(ch_3_pin, OFF);
      }
      if (ch_4[(h * 60 + m) / 30] == 1)
      {
        digitalWrite(ch_4_pin, ON);
      } else {
        digitalWrite(ch_4_pin, OFF);
      }
      if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
      {
        if ((co2[0] * 60 + co2[1]) <= (h * 60 + m) && (h * 60 + m) < (co2[2] * 60 + co2[3]))
        {
          analogWrite(co2_pin, CO2_ONN);
        } else {
          analogWrite(co2_pin, CO2_OFF);
        }
      } else {
        digitalWrite(co2_pin, CO2_OFF);
      }
      /////////////////////////////////каналы
      /////////////////////////////////Кормежка
      if (((food[0] != 0) && (h == food[0]) && (m == food[1]) && s <= food[4]) || ((food[2] != 0) && (h == food[2]) && (m == food[3]) && s <= food[4]))
      {
        //  lcd.setCursor(8, 1);
        //   lcd.print("F");
        analogWrite(food_pin, 255);
      } else {
        //  lcd.setCursor(8, 1);
        //   lcd.print("_");
        analogWrite(food_pin, 0);
      }
      /////////////////////////////////Кормежка

      /////////////////////переключение главных экранов
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 2) menu_val_2 = 0;
        lcd.clear();//обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 1;
        lcd.clear();//обработка поворота энкодера без нажатия - проход по меню
      }
      switch (menu_val_2) {
        /////****
        case 0:
          lcd.setCursor(0, 1);
          lcd.write(3);
          ///////////////// CH1
          if (ch_1[(h * 60 + m) / 30] == 1)
          {
            lcd.setCursor(1, 1);
            lcd.print("1") ;
          }
          else
          {
            lcd.setCursor(1, 1);
            lcd.print("_") ;
          }
          ///////////////// CH1
          ///////////////// CH2
          if (ch_2[(h * 60 + m) / 30] == 1)
          {
            lcd.setCursor(2, 1);
            lcd.print("2") ;
          }
          else
          {
            lcd.setCursor(2, 1);
            lcd.print("_") ;
          }
          ///////////////// CH2
          ///////////////// CH3
          if (ch_3[(h * 60 + m) / 30] == 1)
          {
            lcd.setCursor(3, 1);
            lcd.print("3") ;
          }
          else
          {
            lcd.setCursor(3, 1);
            lcd.print("_") ;
          }
          ///////////////// CH3
          ///////////////// CH4
          if (ch_4[(h * 60 + m) / 30] == 1)
          {
            lcd.setCursor(4, 1);
            lcd.print("4") ;
          }

          else
          {
            lcd.setCursor(4, 1);
            lcd.print("_") ;
          }
          ///////////////// CH4
          /////////////////со2  rtc[2] - hour   rtc[1] - minute
          if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
          {
            if ((co2[0] * 60 + co2[1]) <= (h * 60 + m) && (h * 60 + m) < (co2[2] * 60 + co2[3]))
            {
              lcd.setCursor(6, 1);
              lcd.write(0);
            }
            else {
              lcd.setCursor(6, 1);
              lcd.print("_") ;
            }
          } else {
            lcd.setCursor(6, 1);
            lcd.print("_") ;
          }
          /////////////////со2
          lcd.setCursor(10, 1);
          if ((Temp_val_led - x_temp_led) > delta_led)
          {
            lcd.print("*") ;
          }
          if ((x_temp_led - Temp_val_led) > delta_led)
          {
            lcd.print(" ") ;
          }
            //////////температура2
      lcd.setCursor(10, 1);
      if ((Temp_val_led - x_temp_led) > delta_led)
      {
          lcd.print("*") ;
        digitalWrite(cool_led_pin, HIGH);
      }
      if ((x_temp_led - Temp_val_led) > delta_led)
      {
         lcd.print(" ") ;
        digitalWrite(cool_led_pin, LOW);
      }

      //////////температура2
          lcd.setCursor(11, 1);
          lcd.print(Temp_val_led, 1);
          lcd.write(223);
          /////////////////////////////////Кормежка
          if (((food[0] != 0) && (h == food[0]) && (m == food[1]) && s <= food[4]) || ((food[2] != 0) && (h == food[2]) && (m == food[3]) && s <= food[4]))
          {
            lcd.setCursor(8, 1);
            lcd.print("F");

          } else {
            lcd.setCursor(8, 1);
            lcd.print("_");
          }
          /////////////////////////////////Кормежка
          break;
        /////****
        case 1:
          lcd.setCursor(1, 1);
          lcd.print(LR) ;
          lcd.setCursor(4, 1);
          lcd.print(LG) ;
          lcd.setCursor(7, 1);
          lcd.print(LB) ;
          lcd.setCursor(10, 1);
          lcd.print(LW) ;
          lcd.setCursor(13, 1);
          lcd.print(LC) ;
          break;
      }
      /////////////////////переключение главных экранов

      break;
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////основной экран
    case 2:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 6) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 5; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:

          motion_screen ("SUN", (int*)rgb_motion, 0);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++rgb_motion[0] >= 24) rgb_motion[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--rgb_motion[0] < 0) rgb_motion[0] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 1:

          motion_screen ("SUN", (int*)rgb_motion, 1);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++rgb_motion[1] >= 60) rgb_motion[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--rgb_motion[1] < 0) rgb_motion[1] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:

          motion_screen ("SUN", (int*)rgb_motion, 2);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++rgb_motion[3] >= 24) rgb_motion[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--rgb_motion[3] < 0) rgb_motion[3] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 3:

          motion_screen ("SUN", (int*)rgb_motion, 3);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++rgb_motion[4] >= 60) rgb_motion[4] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--rgb_motion[4] < 0) rgb_motion[4] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 4:

          motion_screen ("SUN", (int*)rgb_motion, 4);

          if (enc1.isRightH()) {
            rgb_motion[2] = rgb_motion[2] + 5;
            if (rgb_motion[2] >= 121) rgb_motion[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb_motion[2] = rgb_motion[2] - 5;
            if (rgb_motion[2] < 0) rgb_motion[2] = 120; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 5:

          motion_screen ("SUN", (int*)rgb_motion, 5);

          if (enc1.isRightH()) {
            rgb_motion[5] = rgb_motion[5] + 5;
            if (rgb_motion[5] >= 121) rgb_motion[5] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb_motion[5] = rgb_motion[5] - 5;
            if (rgb_motion[5] < 0) rgb_motion[5] = 120; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

      }

      break;
    /////////////////////////////////////дневная пауза
    case 3:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 4) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 3; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:
          lcd.setCursor(0, 0);
          lcd.print("DAY PAUSE");
          lcd.setCursor(10, 0);
          if ((day_pause[0] * 60 + day_pause[1]) < (day_pause[2] * 60 + day_pause[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.write(126);
          lcd.setCursor(1, 1);
          if (day_pause[0] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (day_pause[1] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (day_pause[2] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (day_pause[3] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[3]);
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++day_pause[0] >= 24) day_pause[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--day_pause[0] < 0) day_pause[0] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }

          break;
        case 1:
          lcd.setCursor(0, 0);
          lcd.print("DAY PAUSE");
          lcd.setCursor(10, 0);
          if ((day_pause[0] * 60 + day_pause[1]) < (day_pause[2] * 60 + day_pause[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (day_pause[0] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[0]);
          lcd.setCursor(3, 1);
          lcd.write(126);
          lcd.setCursor(4, 1);
          if (day_pause[1] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (day_pause[2] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (day_pause[3] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++day_pause[1] >= 60) day_pause[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--day_pause[1] < 0) day_pause[1] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:
          lcd.setCursor(0, 0);
          lcd.print("DAY PAUSE");
          lcd.setCursor(10, 0);
          if ((day_pause[0] * 60 + day_pause[1]) < (day_pause[2] * 60 + day_pause[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (day_pause[0] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (day_pause[1] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[1]);
          lcd.setCursor(6, 1);
          lcd.write(126);
          lcd.setCursor(7, 1);
          if (day_pause[2] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (day_pause[3] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++day_pause[2] >= 24) day_pause[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--day_pause[2] < 0) day_pause[2] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 3:
          lcd.setCursor(0, 0);
          lcd.print("DAY PAUSE");
          lcd.setCursor(10, 0);
          if ((day_pause[0] * 60 + day_pause[1]) < (day_pause[2] * 60 + day_pause[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (day_pause[0] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (day_pause[1] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (day_pause[2] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[2]);
          lcd.setCursor(9, 1);
          lcd.write(126);
          lcd.setCursor(10, 1);
          if (day_pause[3] < 10) {
            lcd.print("0");
          }
          lcd.print(day_pause[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++day_pause[3] >= 60) day_pause[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--day_pause[3] < 0) day_pause[3] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

      }
      break;
      break;
    ////////////////////////////////////дневная пауза
    ////////////////////////////////////RGB
    case 4:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 5) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 4; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:
          lcd.setCursor(0, 0);
          lcd.write(126);
          lcd.print("R  G  B  W  C ");

          if (enc1.isRightH()) {
            rgb[0] = rgb[0] + 5;
            if (rgb[0] >= 101) rgb[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb[0] = rgb[0] - 5;
            if (rgb[0] < 0) rgb[0] = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          rgb_screen();
          break;
        case 1:
          lcd.setCursor(0, 0);
          lcd.print(" R ");
          lcd.write(126);
          lcd.print("G  B  W  C ");

          rgb_screen();


          /*    if (enc1.isRightH()) {
                rgb[1] = rgb[1] + 5;
                if (rgb[1] >= 101) rgb[1] = 5;  //обработка поворота энкодера без нажатия - проход по меню
              } else if (enc1.isLeftH()) {
                rgb[1] = rgb[1] - 5;
                if (rgb[1] <= 0) rgb[1] = 100; //обработка поворота энкодера без нажатия - проход по меню
              }*/

          if (enc1.isRightH()) {
            rgb[1] = rgb[1] + 5;
            if (rgb[1] >= 101) rgb[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb[1] = rgb[1] - 5;
            if (rgb[1] < 0) rgb[1] = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:
          lcd.setCursor(0, 0);
          lcd.print(" R  G ");
          lcd.write(126);
          lcd.print("B  W  C ");
          rgb_screen();


          if (enc1.isRightH()) {
            rgb[2] = rgb[2] + 5;
            if (rgb[2] >= 101) rgb[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb[2] = rgb[2] - 5;
            if (rgb[2] < 0) rgb[2] = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 3:
          lcd.setCursor(0, 0);
          lcd.print(" R  G  B ");
          lcd.write(126);
          lcd.print("W  C ");
          rgb_screen();


          if (enc1.isRightH()) {
            rgb[3] = rgb[3] + 5;
            if (rgb[3] >= 101) rgb[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb[3] = rgb[3] - 5;
            if (rgb[3] < 0) rgb[3] = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 4:
          lcd.setCursor(0, 0);
          lcd.print(" R  G  B  W ");
          lcd.write(126);
          lcd.print("C ");
          rgb_screen();


          if (enc1.isRightH()) {
            rgb[4] = rgb[4] + 5;
            if (rgb[4] >= 101) rgb[4] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            rgb[4] = rgb[4] - 5;
            if (rgb[4] < 0) rgb[4] = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
      }


      break;
    ////////////////////////////////////RGB

    //////////////////////////**************************************************************************луна*************

    case 5:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 6) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 5; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:

          motion_screen ("MOON", (int*)moon_motion, 0);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++moon_motion[0] >= 24) moon_motion[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--moon_motion[0] < 0) moon_motion[0] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 1:

          motion_screen ("MOON", (int*)moon_motion, 1);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++moon_motion[1] >= 60) moon_motion[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--moon_motion[1] < 0) moon_motion[1] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:

          motion_screen ("MOON", (int*)moon_motion, 2);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++moon_motion[3] >= 24) moon_motion[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--moon_motion[3] < 0) moon_motion[3] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 3:

          motion_screen ("MOON", (int*)moon_motion, 3);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++moon_motion[4] >= 60) moon_motion[4] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--moon_motion[4] < 0) moon_motion[4] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 4:

          motion_screen ("MOON", (int*)moon_motion, 4);

          if (enc1.isRightH()) {
            moon_motion[2] = moon_motion[2] + 5;
            if (moon_motion[2] >= 121) moon_motion[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            moon_motion[2] = moon_motion[2] - 5;
            if (moon_motion[2] < 0) moon_motion[2] = 120; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 5:

          motion_screen ("MOON", (int*)moon_motion, 5);

          if (enc1.isRightH()) {
            moon_motion[5] = moon_motion[5] + 5;
            if (moon_motion[5] >= 121) moon_motion[5] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            moon_motion[5] = moon_motion[5] - 5;
            if (moon_motion[5] < 0) moon_motion[5] = 120; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

      }

      break;
    case 6:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 5) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 4; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:
          lcd.setCursor(0, 0);
          lcd.print("MOON");
          lcd.setCursor(0, 1);
          lcd.print("COLOR");
          lcd.setCursor(10, 0);
          lcd.write(126);
          lcd.print("B  R");

          if (enc1.isRightH()) {
            pwm_moon_b = pwm_moon_b + 1;
            if (pwm_moon_b >= 101) pwm_moon_b = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            pwm_moon_b = pwm_moon_b - 1;
            if (pwm_moon_b < 0) pwm_moon_b = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          lcd.setCursor(10, 1);
          lcd.print(pwm_moon_b);
          if (pwm_moon_b < 10) {
            lcd.print("  ");
          }
          if (pwm_moon_b < 100) {
            lcd.print(" ");
          }
          lcd.setCursor(13, 1);
          lcd.print(pwm_moon_r);
          if (pwm_moon_r < 10) {
            lcd.print("  ");
          }
          if (pwm_moon_r < 100) {
            lcd.print(" ");
          }
          break;
        case 1:
          lcd.setCursor(10, 0);
          lcd.print(" R ");
          lcd.write(126);
          lcd.print("B ");

          lcd.setCursor(10, 1);
          lcd.print(pwm_moon_b);
          if (pwm_moon_b < 10) {
            lcd.print("  ");
          }
          if (pwm_moon_b < 100) {
            lcd.print(" ");
          }
          lcd.setCursor(13, 1);
          lcd.print(pwm_moon_r);
          if (pwm_moon_r < 10) {
            lcd.print("  ");
          }
          if (pwm_moon_r < 100) {
            lcd.print(" ");
          }

          if (enc1.isRightH()) {
            pwm_moon_r = pwm_moon_r + 1;
            if (pwm_moon_r >= 101) pwm_moon_r = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            pwm_moon_r = pwm_moon_r - 1;
            if (pwm_moon_r < 0) pwm_moon_r = 100; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

      }


      break;

    //////////////////////////**************************************************************************луна*************

    ////////////////////////****************************************************************температура LED
    case 7:
      lcd.setCursor(0, 0);
      lcd.print("LED T>");
      lcd.print(Temp_val_led, 1);
      lcd.write(223);
      lcd.setCursor(12, 0);
      lcd.print(x_temp_led);
      lcd.write(223);
      lcd.setCursor(0, 1);
      lcd.print("+- ");
      lcd.setCursor(5, 1);
      lcd.print(delta_led, 1);
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 2) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 1; //обработка поворота энкодера без нажатия - проход по меню
      }
      switch (menu_val_2)
      {
        case 0:
          lcd.setCursor(11, 0);
          lcd.write(126);
          lcd.setCursor(3, 1);
          lcd.print(" ");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++x_temp_led >= 61) x_temp_led = 25;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--x_temp_led < 25) x_temp_led = 60; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 1:
          lcd.setCursor(11, 0);
          lcd.print(" ");
          lcd.setCursor(3, 1);
          lcd.write(126);
          //  if ((delta <0)||(delta>2)){delta=0;}
          if (enc1.isRightH()) {
            delta_led = delta_led + 0.1;
            if (delta_led > 5) {
              delta_led = 0;
            }
          } else if (enc1.isLeftH()) {
            delta_led = delta_led - 0.1;
            if (delta_led < 0) {
              delta_led = 5;
            }
          }
          break;

      }
      break;
    ///////////////////////*****************************************************************температура LED

    //////////////////////////**************************************************************************КОРМ*************
    case 8:
      lcd.setCursor(0, 0);
      lcd.print("Food time");
      lcd.setCursor(11, 0);
      if (food[0] == 0) {
        lcd.print("XX");
      } else {
        if (food[0] < 10) {
          lcd.print("0");
        }
        lcd.print(food[0]);
      }
      lcd.setCursor(14, 0);

      if (food[1] < 10) {
        lcd.print("0");
      }
      lcd.print(food[1]);
      /////////////////
      lcd.setCursor(0, 1);
      lcd.print("on");
      lcd.setCursor(3, 1);

      lcd.print(food[4]);
      if (food[1] < 10) {
        lcd.print(" ");
      }
      if (food[1] < 100) {
        lcd.print(" ");
      }
      lcd.setCursor(6, 1);
      lcd.print("sec");
      /////////////////
      lcd.setCursor(11, 1);
      if (food[2] == 0) {
        lcd.print("XX");
      } else {
        if (food[2] < 10) {
          lcd.print("0");
        }
        lcd.print(food[2]);
      }
      lcd.setCursor(14, 1);

      if (food[3] < 10) {
        lcd.print("0");
      }
      lcd.print(food[3]);
      ////////////////////////
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 5) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 4; //обработка поворота энкодера без нажатия - проход по меню
      }
      switch (menu_val_2)
      {
        case 0:
          lcd.setCursor(10, 0);
          lcd.print(" ");
          lcd.setCursor(13, 0);
          lcd.print(":");
          lcd.setCursor(10, 1);
          lcd.print(" ");
          lcd.setCursor(13, 1);
          lcd.print(":");
          lcd.setCursor(2, 1);
          lcd.write(126);
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++food[4] >= 60) food[4] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--food[4] < 0) food[4] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 1:
          lcd.setCursor(10, 0);
          lcd.write(126);
          lcd.setCursor(13, 0);
          lcd.print(":");
          lcd.setCursor(10, 1);
          lcd.print(" ");
          lcd.setCursor(13, 1);
          lcd.print(":");
          lcd.setCursor(2, 1);
          lcd.print(":");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++food[0] >= 24) food[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--food[0] < 0) food[0] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:
          lcd.setCursor(10, 0);
          lcd.print(" ");
          lcd.setCursor(13, 0);
          lcd.write(126);
          lcd.setCursor(10, 1);
          lcd.print(" ");
          lcd.setCursor(13, 1);
          lcd.print(":");
          lcd.setCursor(2, 1);
          lcd.print(":");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++food[1] >= 60) food[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--food[1] < 0) food[1] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

        case 3:
          lcd.setCursor(10, 0);
          lcd.print(" ");
          lcd.setCursor(13, 0);
          lcd.print(":");
          lcd.setCursor(10, 1);
          lcd.write(126);
          lcd.setCursor(13, 1);
          lcd.print(":");
          lcd.setCursor(2, 1);
          lcd.print(":");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++food[2] >= 24) food[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--food[2] < 0) food[2] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 4:
          lcd.setCursor(10, 0);
          lcd.print(" ");
          lcd.setCursor(13, 0);
          lcd.print(":");
          lcd.setCursor(10, 1);
          lcd.print(" ");
          lcd.setCursor(13, 1);
          lcd.write(126);
          lcd.setCursor(2, 1);
          lcd.print(":");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++food[3] >= 60) food[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--food[3] < 0) food[3] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;


      }
      break;
    //////////////////////////**************************************************************************КОРМ*************

    case 9:
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 4) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 3; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2) {
        case 0:
          lcd.setCursor(0, 0);
          lcd.print("Co2");
          lcd.setCursor(10, 0);
          if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.write(126);
          lcd.setCursor(1, 1);
          if (co2[0] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (co2[1] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (co2[2] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (co2[3] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[3]);
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++co2[0] >= 24) co2[0] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--co2[0] < 0) co2[0] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }

          break;
        case 1:
          lcd.setCursor(0, 0);
          lcd.print("Co2");
          lcd.setCursor(10, 0);
          if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (co2[0] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[0]);
          lcd.setCursor(3, 1);
          lcd.write(126);
          lcd.setCursor(4, 1);
          if (co2[1] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (co2[2] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (co2[3] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++co2[1] >= 60) co2[1] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--co2[1] < 0) co2[1] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 2:
          lcd.setCursor(0, 0);
          lcd.print("Co2");
          lcd.setCursor(10, 0);
          if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (co2[0] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (co2[1] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[1]);
          lcd.setCursor(6, 1);
          lcd.write(126);
          lcd.setCursor(7, 1);
          if (co2[2] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[2]);
          lcd.setCursor(9, 1);
          lcd.print(":");
          lcd.setCursor(10, 1);
          if (co2[3] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++co2[2] >= 24) co2[2] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--co2[2] < 0) co2[2] = 23; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 3:
          lcd.setCursor(0, 0);
          lcd.print("Co2");
          lcd.setCursor(10, 0);
          if ((co2[0] * 60 + co2[1]) < (co2[2] * 60 + co2[3]))
          {
            lcd.print("ON ");
          } else {
            lcd.print("OFF");
          }
          lcd.setCursor(0, 1);
          lcd.print(" ");
          lcd.setCursor(1, 1);
          if (co2[0] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[0]);
          lcd.setCursor(3, 1);
          lcd.print(":");
          lcd.setCursor(4, 1);
          if (co2[1] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[1]);
          lcd.setCursor(6, 1);
          lcd.print("-");
          lcd.setCursor(7, 1);
          if (co2[2] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[2]);
          lcd.setCursor(9, 1);
          lcd.write(126);
          lcd.setCursor(10, 1);
          if (co2[3] < 10) {
            lcd.print("0");
          }
          lcd.print(co2[3]);

          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++co2[3] >= 60) co2[3] = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--co2[3] < 0) co2[3] = 59; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;

      }

      break;
      break;
    case 10:
      output_48 ((int8_t*)ch_1, ch_1_name);
      break;
    case 11:
      output_48 ((int8_t*)ch_2, ch_2_name);
      break;
    case 12:
      output_48 ((int8_t*)ch_3, ch_3_name);
      break;
    case 13:
      output_48 ((int8_t*)ch_4, ch_4_name);
      break;
    case 14:
      lcd.setCursor(0, 0);
      lcd.print("Temp>");
      lcd.print(Temp_val, 1);
      lcd.write(223);
      lcd.setCursor(12, 0);
      lcd.print(x_temp);
      lcd.write(223);
      lcd.setCursor(0, 1);
      lcd.print("+- ");
      lcd.setCursor(5, 1);
      lcd.print(delta, 1);
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 2) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 1; //обработка поворота энкодера без нажатия - проход по меню
      }
      switch (menu_val_2)
      {
        case 0:
          lcd.setCursor(11, 0);
          lcd.write(126);
          lcd.setCursor(3, 1);
          lcd.print(" ");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++x_temp >= 41) x_temp = 19;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--x_temp < 19) x_temp = 40; //обработка поворота энкодера без нажатия - проход по меню
          }
          break;
        case 1:
          lcd.setCursor(11, 0);
          lcd.print(" ");
          lcd.setCursor(3, 1);
          lcd.write(126);
          //  if ((delta <0)||(delta>2)){delta=0;}
          if (enc1.isRightH()) {
            delta = delta + 0.1;
            if (delta > 2) {
              delta = 0;
            }
          } else if (enc1.isLeftH()) {
            delta = delta - 0.1;
            if (delta < 0) {
              delta = 2;
            }
          }
          break;

      }
      break;
    case 15:
      lcd.setCursor(0, 0);
      lcd.print("Time set ");
      lcd.setCursor(10, 0);
      if (h < 10) {
        lcd.print("0");
      }
      lcd.print(h);
      lcd.setCursor(13, 0);
      if (m < 10) {
        lcd.print("0");
      }
      lcd.print(m);
      lcd.setCursor(0, 1);
      lcd.print("LCD ");
      lcd.setCursor(5, 1);

      switch (lcd_on)
      {
        case 0:
          lcd.print("AUTO");
          break;
        case 1:
          lcd.print("OFF ");
          break;
        case 2:
          lcd.print("ON  ");
          break;

      }
      if (enc1.isRight()) {
        //  lcd.clear();
        if (++menu_val_2 >= 3) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
      } else if (enc1.isLeft()) {
        //   lcd.clear();
        if (--menu_val_2 < 0) menu_val_2 = 2; //обработка поворота энкодера без нажатия - проход по меню
      }

      switch (menu_val_2)
      {
        case 0:
          lcd.setCursor(9, 0);
          lcd.write(126);
          lcd.setCursor(12, 0);
          lcd.print(":");
          lcd.setCursor(4, 1);
          lcd.print(" ");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++h >= 24) h = 0;
            setHour(h);
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--h < 0) h = 23;
            setHour(h);
          }
          break;
        case 1:
          lcd.setCursor(9, 0);
          lcd.print(" ");
          lcd.setCursor(12, 0);
          lcd.write(126);
          lcd.setCursor(4, 1);
          lcd.print(" ");
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++m >= 60) m = 0;
            setMinute(m);
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--m < 0) m = 59;
            setMinute(m);
          }

          break;
        case 2:
          lcd.setCursor(9, 0);
          lcd.print(" ");
          lcd.setCursor(12, 0);
          lcd.print(":");
          lcd.setCursor(4, 1);
          lcd.write(126);
          if (enc1.isRightH()) {
            //  lcd.clear();
            if (++lcd_on >= 3) lcd_on = 0;  //обработка поворота энкодера без нажатия - проход по меню
          } else if (enc1.isLeftH()) {
            //   lcd.clear();
            if (--lcd_on < 0) lcd_on = 2; //обработка поворота энкодера без нажатия - проход по меню
          }
          /*   if (lcd_on[1] <= lcd_on[0]) {
               lcd_on[0] = lcd_on[1] - 1;
             }*/
          break;
      }
      break;
    case 16:
      lcd.setCursor(0, 0);
      lcd.print("aquakmv.com");
      lcd.setCursor(0, 1);
      lcd.print(VER);
      break;

  }
}
