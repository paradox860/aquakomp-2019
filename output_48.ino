void output_48 (int8_t *p, char *p_name) {
  lcd.setCursor(0, 0);
  lcd.print(p_name);
  lcd.setCursor(5, 0);
  if (hr % 10 % 2 == 0) {
    if (hr / 2 < 10) {
      lcd.print("0"); //часы
    }
    lcd.print(hr / 2);
    lcd.print(":00");
  }
  else {
    if ((hr - 1) / 2 < 10) {
      lcd.print("0");
    }
    lcd.print((hr - 1) / 2);
    lcd.print(":30");
  }


  lcd.print("-");
  lcd.setCursor(11, 0);
  if ((hr + 1) % 10 % 2 == 0) {

    if (((hr + 1) / 2) < 10) {
      lcd.print("0");
    }
    lcd.print((hr + 1) / 2);
    lcd.print(":00");
  }
  else {
    if ((hr / 2) < 10) {
      lcd.print("0");
    }
    lcd.print(hr / 2);
    lcd.print(":30");
  }


  lcd.setCursor(0, 1);
  lcd.print("    Set > ");
  switch (p[hr]) {
    case 0:
      lcd.print("OFF");
      break;
    case 1:
      lcd.print("ON ");
      break;
    default:
      lcd.print("---");
  };
  if (enc1.isRight()) {
    //  lcd.clear();
    if (++hr >= 48) hr = 0;  //обработка поворота энкодера без нажатия - проход по меню
  } else if (enc1.isLeft()) {
    //   lcd.clear();
    if (--hr < 0) hr = 47; //обработка поворота энкодера без нажатия - проход по меню
  }
  if (enc1.isRightH() || enc1.isLeftH() )
  {
    if (p[hr] == 0)
    {
      p[hr] = 1;
    } else {
      p[hr] = 0;
    }
  }
}
